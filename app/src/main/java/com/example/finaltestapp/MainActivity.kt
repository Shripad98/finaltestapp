package com.example.finaltestapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.finaltestlibrary.TestClassFinal

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val text : TextView = findViewById(R.id.test)
        val testClassFinal  = TestClassFinal()
        text.setText(testClassFinal.getString())
    }
}